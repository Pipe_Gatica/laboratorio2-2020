#!/usr/bin/env python3
# -*- coding:utf-8 -*-

def nuevo_dic(dic):

    print('Primer diccionario\n', dic)

    # Tomamos el valor de su key
    value = dic.get('Histidine')

    # Lo pasamos a una lista para poder separarlos
    list(value)
    primero = value[0] + value[1]
    segundo = value[2] + value[3]
    tercero = value[4] + value[5]
    cuarto = value[6] + value[7]

    # Creamos un nuevo diccionario con el orden que se pidio
    new_dic = {primero: 'Histidine',
               segundo: 'Histidine',
               tercero: 'Histidine',
               cuarto: 'Histidine'}
    return new_dic


if __name__ == '__main__':

    dic = {'Histidine': 'C6H9N3O2'}
    final = nuevo_dic(dic)
    print('\nNuevo diccionario\n', final)
