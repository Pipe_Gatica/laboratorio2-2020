#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import json


#Guardamos en nuestra base de datos la informacion
def save_file(dic):
  with open("aminoacidos.txt", 'w') as file:
    json.dump(dic, file)

# Cargamos la informacion almacenada en nuestra base de datos
# cuando sea necesario
def load_file():
  with open("aminoacidos.txt", 'r') as file:
    dic = json.load(file)
  return dic


def ingresar(dic):

    # Hay que cargar los archivos ya ingresados, de lo contrario, los archivos
    # que ya estaban guardados, se borran
    dic = load_file()
    nombre_aminoacido = str(input('\nIngrese el nombre de su aminoacido: '))
    formula_molecular = str(input('Ingrese su formula molecular: '))

    dic[nombre_aminoacido] = formula_molecular
    save_file(dic)


def buscar(dic):

    dic = load_file()
    print('\nPresione')
    print('1 para buscar por el nombre del aminoacido')
    print('2 para buscar por la formula molecular')
    opt = int(input('Ingrese una opcion: '))


    # Pasamos las claves y los valores a listas diferentes
    claves = list(dic.keys())
    valores = list(dic.values())

    if opt == 1:
        nombre_aminoacido = str(input('Ingrese el nombre del aminoacido a buscar: '))

        valor = claves.index(nombre_aminoacido)
        value = valores[valor]
        print('---> La formula molecular es {0} y se encuentra en el puesto numero {1}'.format(value, valor))


    if opt == 2:
        formula_aminoacido = str(input('Formula molecular de aminoacido a buscar: '))

        # Guardamos el indice de la formula del aminoacido (value) que estamos
        # buscando el cual debe ser el mismo que el de su llave (key)
        valor = valores.index(formula_aminoacido)
        key = claves[valor]
        print('---> El nombre del aminoacido es {0} y se encuentra en el puesto numero {1}'.format(key, valor))


def editar(dic):

    dic = load_file()

    print('\nPresione')
    print('1 para editar el nombre')
    print('2 para editar la formula molecular')
    opt = int(input('Ingrese una opcion: '))

    if opt == 1:
        nombre_aminoacido = str(input('Ingrese el nombre del aminoacido a editar: '))
        newf = str(input('Ingrese el nuevo nombre: '))
        formula_molecular = dic[nombre_aminoacido]

        # De esta manera no cambia el nombre
        # pero se agrega un nueva aminoacido al final de la lista
        # con su nuevo nombre y la misma formula molecular
        del dic[nombre_aminoacido]
        dic[newf] = formula_molecular

    if opt == 2:
        nombre_aminoacido = str(input('Ingrese el nombre del aminoacido a editar: '))
        newm = str(input('Ingrese la nueva formula molecular: '))

        # Simplemente reemplazamos el valor de su clave de manera simple
        dic[nombre_aminoacido] = newm

    save_file(dic)


def eliminar(dic):

    dic = load_file()

    aminoacido = str(input('\nIngrese nombre de aminoacido a eliminar: '))

    # Le pedimos al usuario el nombre para eliminarlo directamente
    del dic[aminoacido]
    save_file(dic)


def mostrar_elementos(dic):

    # Esta funcion solamente carga los datos guardados y los muestra
    dic = load_file()

    print('\nTodos los elementos guardados en nuestra base de datos\n', dic)


def main():

    dic = {}
    while True:
        print('\n------------------------------------')
        print('********** MENU PRINCIPAL **********')
        print('Elija')
        print('i para ingresar un aminoacido')
        print('b para buscar en la formula molecular')
        print('e para editar')
        print('d para eliminar un aminoacido')
        print('m para mostrar todos los elementos')
        print('s para salir')
        opcion = input('Ingrese una opcion: ')

        if opcion.upper() == "I":
            dic = ingresar(dic)

        elif opcion.upper() == "B":
            buscar(dic)

        elif opcion.upper() == "E":
            editar(dic)

        elif opcion.upper() == "D":
            eliminar(dic)

        elif opcion.upper() == "M":
            mostrar_elementos(dic)

        elif opcion.upper() == "S":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass


if __name__ == '__main__':
    main()
